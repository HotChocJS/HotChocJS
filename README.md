# HotChocJS

HotChocJS is a tool for dimming your website so your viewers can sit back, relax and have a hot chocolate while not being blinded by their computer.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Node.js & npm.js
```

### Installing

Lets start by installing Node if you haven't already.
Download at: ``` https://nodejs.org/en/download/ ```

Now use NPM to install HotChocJS.

```
npm install hotchocjs
```

### Alternatives

Incase you do not wish to deal with node much or the node modules folder,
I have prepared a bower package for you:
to get the package: type ```bower install hotchocjs```

## Built With

* [Jquery](https://jquery.com/) - The Javascript framework used
* [SizzleJS](https://sizzlejs.com/) - Javascript Selector Library
* [Node](https://nodejs.org/) - Dependency Creation
* [NPM](https://npmjs.com/) - Dependency Publishing
* [Bower](https://bower.io/) - Dependency Creation, Publishing
* [GitLab](https://gitlab.com/) - Dependency Management

## Contributing

Please see: [CONTRIBUTING.MD](https://gitlab.com/HotChocJS/HotChocJS/blob/master/CONTRIBUTING.md) If you would like to contibute to the development of [HotChocJS](https://gitlab.com/HotChocJS/HotChocJS), please create pull requests.

## Versioning

For the versions available, see the [tags on this repository](https://gitlab.com/HotChocJS/HotChocJS/tags). 

## Authors

* **Christian Barton Randall** - *Main Code* - [Pigcake999](https://gitlab.com/pigcake999)
* **Javier Sierra** - *Help with development.* - [Sierrica](https://gitlab.com/sierrica)

## License

This project is licensed under the Apache Liscene 2.0- see the [LICENSE.txt](https://gitlab.com/HotChocJS/HotChocJS/blob/master/LISCENCE.txt) file for details

## Acknowledgments

* JS Foundation for Jquery,
* My Burning eyes looking at a computer screen at night,
* My family for pushing me along :)
